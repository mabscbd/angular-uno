import { Component, OnInit, Input } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
   @Input() destino: destinoViaje;
   @HostBinding('attr.class') ccsClass = 'col-md-4'
  constructor() {}

  ngOnInit(): void {
  }

}
